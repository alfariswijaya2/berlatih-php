<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function index () {

        //$cast = DB::table('cast')->get();
        $cast = Cast::all();

        return view ('casts.index',compact('cast'));
    }

    public function create () {
        return view ('casts.create');
        }

    public function store (Request $request) {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio'  => 'required',
        ]);
        /*$query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);*/
            
        /*
        $cast = new Cast;
        $cast->nama = $request["nama"];
        $cast->umur = $request["umur"];
        $cast->bio = $request["bio"];
        $cast->save();
        */

        $cast = Cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio"  => $request["bio"]
        ]);
        
        return redirect ('/cast')->with('success','Cast berhasil disimpan');
    }

    public function show ($id) {
        //$cast = DB::table('cast')->where('id',$id)->first();
        
        $cast = Cast::find($id);
        return view ('casts.show',compact('cast'));
    }

    public function edit ($id) {
        //$cast = DB::table('cast')->where('id',$id)->first();
        
        $cast = Cast::find($id);
        return view ('casts.edit',compact('cast'));
    }

    public function update ($id, Request $request) {
        /*
        
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio'  => 'required',
        ]);

        $query = DB::table('cast')->where('id',$id)->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);
        */
        
        $update = Cast::where('id',$id)->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect ('/cast');
    }

    public function destroy ($id) {
        //$query = DB::table('cast')->where('id',$id)->delete();
        
        Cast::destroy($id);
        return redirect('/cast');
    }
}
