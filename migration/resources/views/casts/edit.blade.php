@extends('adminlte.master')



@section('content')
<h1>Edit Cast {{$cast->id}}</h1>
<div class = "ml-4 mt-4 mr-4">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Data Cast</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/cast/{{$cast->id}}" method ="POST">
              @csrf
              @method('put')
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" class="form-control" name="nama" value = "{{$cast->nama}}" placeholder="Nama : ">
                    @error('nama')
                    <div class="alert alert-danger">
                    {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="body">Umur</label>
                    <input type="text" class="form-control" name="umur"  value = "{{$cast->umur}}" placeholder="Umur : ">
                    @error('umur')
                    <div class="alert alert-danger">
                    {{ $message }}
                    </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="body">Bio</label>
                    <input type="text" class="form-control" name="bio"  value = "{{$cast->bio}}" placeholder="Bio : ">
                    @error('bio')
                    <div class="alert alert-danger">
                    {{ $message }}
                    </div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
</div>

@endsection