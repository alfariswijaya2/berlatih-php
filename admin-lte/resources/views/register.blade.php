<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/result" method="POST">
        @csrf
        <label>First Name :</label> <br><br>
        <input type="text" name="nama"> <br><br>
        <label >Last Name :</label> <br><br>
        <input type="text" name="nama_akhir"> <br><br>
        <label >Gender: </label> <br><br>
        <input type="radio" name="JK">Man  <br>
        <input type="radio" name="JK">Woman <br>
        <input type="radio" name="JK">Other  <br><br>
        <label >Nationally:</label>
        <select name="nationally">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select> <br><br>  
        <label>Language Spoken: </label> <br><br>
        <input type="checkbox">Bahasa Indonesia  <br>
        <input type="checkbox">English           <br>
        <input type="checkbox">Arabic            <br>
        <input type="checkbox">Japanese          <br><br>
        <label>Bio: </label>                     <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">

    </form>
</body>
</html>