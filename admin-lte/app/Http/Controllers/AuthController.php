<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function result(){
        return view('result');
    }
    public function result_post(Request $request){
        $nama = $request["nama"];
        $nama_akhir = $request["nama_akhir"];
        return view('result',["nama" => $nama,"nama_akhir"=>$nama_akhir]) ;
    }
}
