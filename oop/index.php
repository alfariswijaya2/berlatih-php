<?php

require ('animal.php');
require ('frog.php');
require ('ape.php');

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>";
echo "legs : $sheep->legs <br>";
echo "cold blooded : $sheep->cold_blooded <br>";

echo "<br>";

$frog = new Frog("buduk");

echo "Name : $frog->name <br>";
echo "legs : $frog->legs <br>";
echo "cold blooded : $frog->cold_blooded <br>";
$frog->jump();

echo "<br>";
echo "<br>";
$ape = new Ape("kera sakti");

echo "Name : $ape->name <br>";
echo "legs : $ape->legs <br>";
echo "cold blooded : $ape->cold_blooded <br>";
$ape->yell();

?>